# <div align="center">Perception Of Violence</div>

#### Nom du groupe : KGB

#### Code du groupe : MAJ23-T3-E

#### Participants :

- Ryan GOURDON
- Romain BOADE
- Yassin KIOUA

## Objectif pédagogique

Le joueur devra se rendre dans plusieurs scénarios où il devra résoudre divers conflits portant sur tous les types de violences possibles, tels que psychologiques, verbales, etc., et tout cela, à titre éducatif. L'objectif est de faire comprendre au joueur que chaque personne peut avoir une perception différente de la violence et que cela peut mener à des fins multiples.

## Type de jeu et mécanismes d'apprentissage

Dans ce jeu, le joueur incarne un enquêteur junior qui sera suivi par son superviseur. Il devra essayer de résoudre les différents conflits. Pour cela, il pourra discuter avec les personnes autour de lui dans la zone, utiliser les fiches que le superviseur lui donnera pour obtenir des premiers antécédents des protagonistes. Dès que le joueur se sentira prêt à régler le conflit, il pourra exprimer son opinion à son superviseur, et il pourra utiliser la méthode qui semble la meilleure au joueur pour régler ce conflit.

Le jeu se déroulera sous forme de trois scénarios (stade, bureau et salle de sport). À la fin de chaque scénario, le joueur obtiendra une note pour la gestion du conflit par rapport à la situation. La moyenne de ses notes lui permettra d'obtenir une note finale pour déterminer s'il sera admis ou refusé en tant qu'enquêteur.

Le joueur devra ainsi se faire sa propre perception de la situation et prendre en compte les émotions et relations qu'ont les personnes autour des protagonistes. Il devra donc essayer de discerner les vraies informations des fausses. Il aura plusieurs moyens de régler les conflits pour chaque situation et devra utiliser le plus adapté à la situation actuelle (demander des excuses, intervenir physiquement, etc.).

## Liste des principales fonctionnalités

- Plateforme de développement : **Godot**

### Fonctionnalité :

- [x] Choix aléatoire des cas de scénarios qui apparaissent
- [x] Résolution de conflit par choix
- [x] Fiche d'information par personnage principal
- [x] Lancement d'une partie
- [x] Système de note avec UI 
- [x] Compte rendu des choix par le superviseur
- [x] Bouton tutoriel
- [x] Menu principal
- [x] Menu dynamique
- [x] Gestion aléatoire des dialogues (NPC)
- [x] Fleche directionnel en fonction du scénario
- [x] Zoom Caméra possible
- [x] Possibilté de se battre si cas choisi
- [x] Menu Option

## Justification du choix de la Licence

Nous avons choisi la licence MIT pour notre jeu, "POV : Perception of Violence", axé sur la compréhension de la violence. Godot, l'éditeur que nous utilisons, est puissant et flexible grâce à cette licence, nous offrant la liberté de distribuer, utiliser et modifier le code source. Elle nous permet de développer le jeu de manière collaborative. En respectant les termes de la licence MIT, nous assurons la compatibilité avec Godot et préservons les droits de l'artiste qui fournit nos assets graphiques, tout en permettant à d'autres de contribuer au projet.

## Captures d'écran

### Capture d'écran du spawn avec flèche directionnel
![Screenshot spawn et fleche](Capture ecran/spawn.png)

### Capture d'écran discussion avec le superviseur
![Screenshot discussion superviseur](Capture ecran/discussion_superviseur.png)

### Capture d'écran des fiches personnages
![Screenshot fiche personnage](Capture ecran/fiche.png)

### Capture d'écran des notes
![Screenshot note](Capture ecran/note.png)

## Lien de téléchargement

Pour ouvrir le projet, il est nécessaire de posséder le logiciel et moteur de jeu Godot Engine, disponible ici en téléchargement : **[Godot Engine](https://godotengine.org/download)**


Vous pouvez cloner l'ensemble du [dépôt Git](https://git.unistra.fr/maj23-t3-e/t3)  ou si cela est plus pratique, vous pouvez également récupérer uniquement le code source en suivant [ce lien](./src) pour poursuivre le projet.

## Procédures d'installation et d'exécution :

### Exécutable :

- ### **Linux** : 
  - Télécharger le dossier [Exécutable Linux](./Executables/Linux/)
  - Ouvrez votre terminal
  - Allez dans le dossier: `cd Linux`
  - Ajout des droits : `chmod 777 kgb.exe.x86_64` et `chmod 777 kgb.sh`
  - Lancez la commande : `./kgb.sh`.
  - Amusez-vous !
  - Pour un bon fonctionnement, lancez la commande `./kgb.sh` depuis le dossier où il se trouve et pas allieurs

- ### **Windows**
  - Ouvrez le dossier [Exécutable Windows](./Executables/Windows/)
  - Lancez **kgb.exe**
  - Amusez-vous

- ### **MacOs**
  - Ouvrez le dossier [Exécutable MacOs](./Executables/MacOS/)
  - Télécharger le zip : `kgb.zip`
  - déziper le en cliquant 2 fois puis déziper
  - mettez les droits sur le fichiers : `chmod 777 t3-gourdon-boade-kioua.command`
  - lancer le script 
  - Amuser vous

### Installation : 

- ### **Windows**
  - Téléchargez l'installeur de [Pov](./Installation/Windows)
  - Lancez `kgb_maj23_t3_e.exe`
  - Suivez les étapes de l'installeur.
  - Lancez **kgb.exe**
  - Amusez-vous !

- ### **MacOs**
  - Téléchargez l'installeur de [Pov](./Installation/MacOs)
  - Lancez `kgb_maj23_t3_e.exe`
  - Suivez les étapes de l'installeur.
  - Lancez **kgb.exe**
  - Amusez-vous !

- ### **Linux**
  - Téléchargez l'installeur de [Pov](./Installation/Linux)
  - Installer la commande wine avec : `sudo apt-get install wine`
  - Lancer ensuite avec la commande : : wine kgb_maj23_t3_e.exe
  - Suivez les étapes de l'installeur.
  - Lancez **kgb.exe**
  - Amusez-vous !